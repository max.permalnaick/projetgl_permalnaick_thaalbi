
#include "defs.h"



void LoadObjGround(ObjetGlManager& _objManager);
void LoadObjBoy(ObjetGlManager& _objManager)  ;
void LoadObjCar(ObjetGlManager& _objManager)  ;
void LoadObjHouse(ObjetGlManager& _objManager);
void LoadObjLamp(ObjetGlManager& _objManager) ;
void LoadObjSun(ObjetGlManager& _objManager)  ;
void LoadObjTree(ObjetGlManager& _objManager) ;




void _chargeObjetsGL(ObjetGlManager& _objManager)
{
    _objManager.mShaders["shaderDefault"] = new Shader ("resources/shaders/defaultvertex.shader"    , "resources/shaders/defaultfragment.shader"    );
    _objManager.mShaders["shaderLight"  ] = new Shader ("resources/shaders/lightvertex.shader"      , "resources/shaders/lightfragment.shader"      );
    _objManager.mShaders["shaderObj"    ] = new Shader ("resources/shaders/objectvertex.shader"     , "resources/shaders/objectfragment.shader"     );
    _objManager.mShaders["shaderBLight" ] = new Shader ("resources/shaders/basiclightvertex.shader" , "resources/shaders/basiclightfragment.shader" );

    LoadObjSun  (_objManager);
    LoadObjGround(_objManager);
    LoadObjHouse(_objManager);
    LoadObjTree (_objManager);
    LoadObjLamp (_objManager);
    LoadObjCar  (_objManager);
    LoadObjBoy  (_objManager);
}






static void _mainLoop_computeInputs(ViewManager& _view, InputManager& _input, ObjetGlManager& _objManager);
static std::vector<int> _mainLoop_donnePersonnage(ObjetGlManager& _objManager);
void _mainLoop(ViewManager& _view, InputManager& _input, ObjetGlManager& _objManager)
{
    // timing
    float lastTime = glfwGetTime();
    int nbFrames = 0;
    _view.SetCamera (glm::vec3(-100.0f, 30.0f, 300.0f));
    _view.SetCamera (glm::vec3(1.2f, 1.0f, 2.0f));

    // scene loop
    while(!glfwWindowShouldClose(_view.mWindow))
    {
        // fps
        float currentTime = glfwGetTime();
        nbFrames++;
        if ( currentTime - lastTime >= 1.0 )
        {
            printf("%f frame/s\n", double(nbFrames));
            nbFrames = 0;
            lastTime+= currentTime;
        }

        _input.PollEvents ();
        _mainLoop_computeInputs(_view,_input,_objManager);
        _view.Update ();

        //dessine les objets avec les effets de lumières
        for(int obj=0; obj< _objManager.mObjets.size ();obj++)
        {
            _objManager.mObjets.at(obj)->Update (_view,currentTime,_objManager.mBuildings);
            for(int light=0; light< _objManager.mSourcesLumieres.size (); light++)
            {
                _objManager.mObjets.at(obj)->ApplyLight (*_objManager.mSourcesLumieres.at(light),light);
            }
            _objManager.mObjets.at(obj)->Draw ();
        }

        _view.BlitToScreen();
    }
}






void _mainLoop_computeInputs(ViewManager& _view, InputManager& _input, ObjetGlManager& _objManager )
{
    static int nObj= 0 ;
    static bool isFollowing=false;
    static std::vector<int> followers = _mainLoop_donnePersonnage(_objManager);
    static int nFollower= 0 ;
    static int nPersonnage= 0 ;

    // de personnage en persoonage
    if(_input.IsKeyPressed (GLFW_KEY_N) )
    {
        if(!_objManager.mObjets.empty ())
        {
            nObj++;
            if(nObj>=_objManager.mObjets.size ())
                nObj=0;
            _view.SetCamera(_objManager.mObjets.at(nObj)->GetCurrPosition()+glm::vec3(0.f,(double)Camera::EYE,0));
            std::cout << "obj("<<nObj<<") id="<<_objManager.mObjets.at(nObj)->mID << std::endl;
        }
    }

    // active le mode suivi
    if(_input.IsKeyPressed (GLFW_KEY_F) )
    {
        isFollowing=!isFollowing;
        if(followers.empty ())
            isFollowing=false;
        else if(isFollowing)
        {
            nFollower++;
            if(nFollower>=followers.size ())
                nFollower=0;
            nPersonnage = followers.at(nFollower);
        }
    }
    if(isFollowing)
    {
        glm::vec3 translation = glm::normalize(_objManager.mObjets.at(nPersonnage)->mTranslation);
        _view.SetCameraTgt (_objManager.mObjets.at(nPersonnage)->GetCurrPosition()-((float)Camera::SPEED)*translation+glm::vec3(0.f,(float)Camera::EYE,0),translation);
        //        std::cout << "follower("<<nPersonnage<<") id="<<_objManager.mObjets.at(nPersonnage)->mID << std::endl;
        //        std::cout << "follower.position"<<_objManager.mObjets.at(nPersonnage)->GetCurrPosition().x << " "<< _objManager.mObjets.at(nPersonnage)->GetCurrPosition().z << std::endl;
        //        std::cout << "follower.translation"<<_objManager.mObjets.at(nPersonnage)->mTranslation.x << " "<< _objManager.mObjets.at(nPersonnage)->mTranslation.z << std::endl;
        //        std::cout << "camera.position"<<_view.mCamera.Position.x << " "<< _view.mCamera.Position.z << std::endl;
    }

    // soleil en pause ou au zenith
    if(_input.IsKeyPressed (GLFW_KEY_M) )
    {
        ObjetGl* soleil = _objManager.Get ("Soleil");
        double fixeSoleil=0;
        soleil->GetVar ("fixeSoleil",fixeSoleil);
        soleil->SetVar ("fixeSoleil",!fixeSoleil);
        soleil->SetVar ("zenithPosition",1.0);
    }
    if(_input.IsKeyPressed (GLFW_KEY_P) )
    {
        ObjetGl* soleil = _objManager.Get ("Soleil");
        double fixeSoleil=0;
        soleil->GetVar ("fixeSoleil",fixeSoleil);
        soleil->SetVar ("fixeSoleil",!fixeSoleil);
        soleil->SetVar ("zenithPosition",0.);
    }
}







std::vector<int> _mainLoop_donnePersonnage(ObjetGlManager& _objManager)
{
    std::vector<int> indices;

    for(int p=0 ; p<_objManager.mCharacters.size (); p++)
    {
        ObjetGl* ptr= _objManager.mCharacters.at(p);
        if(ptr)
        {
            for(int i=0 ;i < _objManager.mObjets.size () ; i++)
            {
                if(_objManager.mObjets.at(i)==ptr){
                    indices.push_back (i);
                    break;
                }
            }
        }
    }
    return indices;
}
