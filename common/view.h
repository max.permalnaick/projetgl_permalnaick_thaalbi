#ifndef VIEW_H
#define VIEW_H

// Std. Includes
#include <stdlib.h>

#include "Camera.h"

/**
 * @brief The View class
 */
class ViewManager
{
public:
    /**
     * @brief mScreenWidth
     */
    int mScreenWidth;
    int mScreenHeight;
    GLFWwindow* mWindow;
    Camera mCamera;

    /**
     * @brief mProjectionMvc
     */
    glm::mat4 mProjectionMvc;
    glm::mat4 mViewMvc;
    float mCycleJourNuit;
    float mCouleurCiel;

    /**
     * @brief View
     * @param _screenWidth
     * @param _screenHeight
     * @param _title
     * @param _cycleJourNuit
     */
    ViewManager(int _screenWidth, int _screenHeight, std::string _title, float _cycleJourNuit=100.0f)
    {

        // Init GLFW
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

        mWindow = glfwCreateWindow(_screenWidth, _screenHeight, _title.c_str (), nullptr, nullptr); // Windowed
        if(mWindow==NULL) throw std::string("Impossible d'initialiser le GLFWwindow");
        glfwMakeContextCurrent(mWindow);

        glfwGetFramebufferSize(mWindow, &mScreenWidth, &mScreenHeight);
        glfwSetInputMode(mWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        glewExperimental = GL_TRUE;
        glewInit();

        // Define the viewport dimensions
        // On recupere les dimensions de la fenetre creee plus haut
        glViewport(0, 0, mScreenWidth, mScreenHeight);
        glEnable(GL_DEPTH_TEST);


        // defaults
        mCouleurCiel=0.f;
        mCycleJourNuit = _cycleJourNuit;
    }
    ~ViewManager()
    {
        glfwDestroyWindow(mWindow);
        glfwTerminate();
    }

    /**
     * @brief Update
     */
    void Update()
    {
        mProjectionMvc = glm::perspective(45.0f, (float)mScreenWidth/(float)mScreenHeight, 0.1f, 100000.0f);
        mViewMvc = mCamera.GetViewMatrix();
    }

    /**
     * @brief SetCamera
     * @param _position
     * @param _up
     * @param _yaw
     * @param _pitch
     */
    void SetCamera(glm::vec3 _position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 _up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat _yaw = Camera::YAW, GLfloat _pitch = Camera::PITCH)
    {
        this->mCamera.Front=(glm::vec3(0.0f, 0.0f, -1.0f));
        this->mCamera.MovementSpeed=(Camera::SPEED);
        this->mCamera.MouseSensitivity=(Camera::SENSITIVTY);
        this->mCamera.deltaTime=(0.0f);
        this->mCamera.lastFrame=(0.0f);

        this->mCamera.Position = _position;
        this->mCamera.WorldUp = _up;
        this->mCamera.Yaw = _yaw;
        this->mCamera.Pitch = _pitch;
        this->mCamera.updateCameraVectors();
    }

    void SetCameraTgt(glm::vec3 _position, glm::vec3 _front)
    {
        this->mCamera.Position = _position;
        this->mCamera.Front = _front;
    }

    /**
     * @brief BlitToScreen
     */
    void BlitToScreen()
    {
        glfwSwapBuffers(mWindow);
        glClearColor( mCouleurCiel/2.f, mCouleurCiel/2.f, mCouleurCiel, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }
};

#endif
