#include "objetglmanager.h"
#include "view.h"


void callback_soleil (ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets);

void LoadObjSun(ObjetGlManager& _objManager)
{
    ObjetGl* Soleil = new ObjetGl("resources/model/Sun/13913_Sun_v2_l3.obj",_objManager.mShaders["shaderLight"]);
    Soleil->mPosition0 = glm::vec3(0.);
    Soleil->mScale0 = glm::vec3(0.01);
    Soleil->mColor = glm:: vec3(1.0);
    Soleil->mCallback = callback_soleil;
    Soleil->mRayon=100.f;
    Soleil->mRad=0.f;
    Soleil->SetLightProporties(0,               //type de lumière
                               glm::vec3(0.),   // offset
                               glm::vec3(0.),   // direction
                               0.3,              // diffuse
                               glm::vec3(0.5), // ambient
                               glm::vec3(1)); // specular

    Soleil->mID="Soleil";
    _objManager.AddLight(Soleil);
    _objManager.mSourcesLumieres.push_back (Soleil);


}







void callback_soleil (ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets)
{
    double fixeSoleil=0.;
    double zenithPosition=0;
    _currentObj.GetVar ("fixeSoleil",fixeSoleil);
    _currentObj.GetVar ("zenithPosition",zenithPosition);

    // temps écoulé depuis la dernière loop
    double lastTime=0.;
    std::cout << "-------_timer=" << _timer << std::endl;
    _currentObj.GetVar ("lastTime",lastTime);
    std::cout << "-------lastTime=" << lastTime << std::endl;
    lastTime = _timer - lastTime;
    if(lastTime<0.2)
        return;
    std::cout << "-------dt=" << lastTime << std::endl;
    _currentObj.SetVar ("lastTime",_timer);

    // nouvel angle de rotation autour de 0 pour simuler le jour et la nuit
    // on peut fixer le soleil pour ne pas bouger et forcer sa position au zénith
    if(!fixeSoleil)
    {
        _currentObj.mRad+=M_PI/_view.mCycleJourNuit;
        if(_currentObj.mRad > 2*M_PI) _currentObj.mRad-=(2*M_PI);
    }
    if(zenithPosition) _currentObj.mRad=M_PI_2;

    // dépalcement seleon l'angle
    _currentObj.mRotation.first += 0.01;
    _currentObj.mRotation.second= glm::vec3(0,1,0);
    _currentObj.mTranslation = glm::vec3(cos(_currentObj.mRad)*2.f, sin(_currentObj.mRad),0.f) * _currentObj.mRayon;

    // l'impact de la lumière est réduite
    _currentObj.mDiffuseStrength=  sin(_currentObj.mRad);
    _currentObj.mDiffuseStrength= (_currentObj.mDiffuseStrength<=0)? 0 : _currentObj.mDiffuseStrength;

    // on assombrit le ciel également
    _view.mCouleurCiel=sin(_currentObj.mRad);
    _currentObj.mEstAllume=true;
    if(_view.mCouleurCiel<=0.15)
    {
        _view.mCouleurCiel=0.15;
        _currentObj.mEstAllume=false;
    }

    //    std::cout << _currentObj.mID << " est =" <<  _currentObj.mEstAllume << std::endl;
}








void callback_moon(ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets)
{

    // temps écoulé depuis la dernière loop
    double lastTime=0.;
    std::cout << "-------_timer=" << _timer << std::endl;
    _currentObj.GetVar ("lastTime",lastTime);
    std::cout << "-------lastTime=" << lastTime << std::endl;
    lastTime = _timer - lastTime;
    if(lastTime<0.2)
        return;
    std::cout << "-------dt=" << lastTime << std::endl;
    _currentObj.SetVar ("lastTime",_timer);

    // nouvel angle de rotation autour de 0 pour simuler le jour et la nuit
    // on peut fixer le soleil pour ne pas bouger et forcer sa position au zénith
    _currentObj.mRad+=M_PI/_view.mCycleJourNuit;
    if(_currentObj.mRad > 2*M_PI) _currentObj.mRad-=(2*M_PI);


    // l'impact de la lumière est réduite
    _currentObj.mDiffuseStrength=  sin(_currentObj.mRad);
    _currentObj.mDiffuseStrength= (_currentObj.mDiffuseStrength<=0)? 0 : _currentObj.mDiffuseStrength;

    //    std::cout << _currentObj.mID << " est =" <<  _currentObj.mEstAllume << std::endl;
}



