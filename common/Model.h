#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
using namespace std;

#include "Mesh.h"
#include "stb_image.h"



class Model
{
public:
    static GLint TextureFromFile(string _path, string _directory="")
    {
        //Generate texture ID and load texture data

        string filename = _path;
        if( !(_directory=="") )
        {
            filename = _directory + '/' + filename;
        }
        GLuint textureID;
        glGenTextures(1, &textureID);

//        int width,height;
//        std::cout << filename.c_str() << std::endl;
//        int channels;
//        unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height,&channels, SOIL_LOAD_RGBA);
//        if(!image)
//            throw std::string("Erreur de chargement image")+filename.c_str();

//        std::cout <<  "Done " << std::endl;
//        // Assign texture to ID
//        glBindTexture(GL_TEXTURE_2D, textureID);
//        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
//        glGenerateMipmap(GL_TEXTURE_2D);

//        // Parameters
//        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
//        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
//        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
//        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//        glBindTexture(GL_TEXTURE_2D, 0);
//        SOIL_free_image_data(image);
//        return textureID;

        int width, height, nrComponents;
        unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
        if (!data)
            throw std::string("Erreur de chargement image ")+filename.c_str();

        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT); // for this tutorial: use GL_CLAMP_TO_EDGE to prevent semi-transparent borders. Due to interpolation it takes texels from next repeat
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, format == GL_RGBA ? GL_CLAMP_TO_EDGE : GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);

        return textureID;
    }

public:
    /*  Model Data  */
    vector<Mesh> mMeshes;
    string mDirectory;
    vector<Texture> mLoadedTextures;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    /*  Functions   */
    // Constructor, expects a filepath to a 3D model.
    Model(string _path)
    {
        this->loadModel(_path);
    }

    //ADD
    Model(vector<Vertex> _vertices, vector<GLuint> _indices, string _path)
    {
         this->mDirectory = _path.substr(0, _path.find_last_of('/'));

        Texture texture;
        texture.id = TextureFromFile(_path, "");
        texture.type = "texture_diffuse";
        texture.path = _path;
        this->mLoadedTextures.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.

        mMeshes.push_back (Mesh(_vertices,_indices,mLoadedTextures));
    }

    //ADD
    ~Model()
    {
        for(int i=0 ;i< mMeshes.size () ; i++)
        {
            mMeshes[i].Delete();
        }
        for(int i=0 ;i< mLoadedTextures.size () ; i++)
        {
            glDeleteBuffers(1, &mLoadedTextures[i].id);
        }
    }
    
    // Draws the model, and thus all its meshes
    void Draw(Shader& _shader)
    {
        for(GLuint i = 0; i < this->mMeshes.size(); i++)
            this->mMeshes[i].Draw(_shader);
    }
    
private:
    
    /*  Functions   */
    // Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    void loadModel(string _path)
    {
        // Read file via ASSIMP
        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(_path, aiProcess_Triangulate | aiProcess_FlipUVs);
        // Check for errors
        if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
        {
            cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
            throw (std::string("ERROR::ASSIMP:: ") + importer.GetErrorString()) ;
        }
        // Retrieve the directory path of the filepath
        this->mDirectory = _path.substr(0, _path.find_last_of('/'));
        
        // Process ASSIMP's root node recursively
        this->processNode(scene->mRootNode, scene);
    }
    
    // Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void processNode(aiNode* _node, const aiScene* _scene)
    {
        // Process each mesh located at the current node
        for(GLuint i = 0; i < _node->mNumMeshes; i++)
        {
            // The node object only contains indices to index the actual objects in the scene.
            // The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
            aiMesh* mesh = _scene->mMeshes[_node->mMeshes[i]];
            this->mMeshes.push_back(this->processMesh(mesh, _scene));
        }
        // After we've processed all of the meshes (if any) we then recursively process each of the children nodes
        for(GLuint i = 0; i < _node->mNumChildren; i++)
        {
            this->processNode(_node->mChildren[i], _scene);
        }
        
    }
    
    Mesh processMesh(aiMesh* _mesh, const aiScene* _scene)
    {
        // Data to fill
        vector<Vertex> vertices;
        vector<GLuint> indices;
        vector<Texture> textures;
        
        // Walk through each of the mesh's vertices
        for(GLuint i = 0; i < _mesh->mNumVertices; i++)
        {
            Vertex vertex;
            glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
            // Positions
            vector.x = _mesh->mVertices[i].x;
            vector.y = _mesh->mVertices[i].y;
            vector.z = _mesh->mVertices[i].z;
            vertex.Position = vector;
            // Normals
            if(_mesh->mNormals)
            {
                vector.x = _mesh->mNormals[i].x;
                vector.y = _mesh->mNormals[i].y;
                vector.z = _mesh->mNormals[i].z;
                vertex.Normal = vector;
            }
            // Texture Coordinates
            if(_mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
            {
                glm::vec2 vec;
                // A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
                // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
                vec.x = _mesh->mTextureCoords[0][i].x;
                vec.y = _mesh->mTextureCoords[0][i].y;
                vertex.TexCoords = vec;
            }
            else
                vertex.TexCoords = glm::vec2(0.0f, 0.0f);
            vertices.push_back(vertex);
        }
        // Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
        for(GLuint i = 0; i < _mesh->mNumFaces; i++)
        {
            aiFace face = _mesh->mFaces[i];
            // Retrieve all indices of the face and store them in the indices vector
            for(GLuint j = 0; j < face.mNumIndices; j++)
                indices.push_back(face.mIndices[j]);
        }
        // Process materials
        if(_mesh->mMaterialIndex >= 0)
        {
            aiMaterial* material = _scene->mMaterials[_mesh->mMaterialIndex];
            // We assume a convention for sampler names in the shaders. Each diffuse texture should be named
            // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
            // Same applies to other texture as the following list summarizes:
            // Diffuse: texture_diffuseN
            // Specular: texture_specularN
            // Normal: texture_normalN
            
            // 1. Diffuse maps
            vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
            textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
            // 2. Specular maps
            vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
            textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
        }
        
        // Return a mesh object created from the extracted mesh data
        return Mesh(vertices, indices, textures);
    }
    
    // Checks all material textures of a given type and loads the textures if they're not loaded yet.
    // The required info is returned as a Texture struct.
    vector<Texture> loadMaterialTextures(aiMaterial* _mat, aiTextureType _type, string _typeName)
    {
        vector<Texture> textures;
        for(GLuint i = 0; i < _mat->GetTextureCount(_type); i++)
        {
            aiString str;
            _mat->GetTexture(_type, i, &str);
            // Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
            GLboolean skip = false;
            for(GLuint j = 0; j < mLoadedTextures.size(); j++)
            {
                if(mLoadedTextures[j].path == str)
                {
                    textures.push_back(mLoadedTextures[j]);
                    skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
                    break;
                }
            }
            if(!skip)
            {   // If texture hasn't been loaded already, load it
                Texture texture;
                texture.id = TextureFromFile(str.C_Str(), this->mDirectory);
                texture.type = _typeName;
                texture.path = str;
                textures.push_back(texture);
                this->mLoadedTextures.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
            }
        }
        return textures;
    }
};

