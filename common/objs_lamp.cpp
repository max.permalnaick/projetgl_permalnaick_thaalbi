#include "objetglmanager.h"
#include "view.h"


void callback_lampadaire(ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets);

void LoadObjLamp(ObjetGlManager& _objManager)
{
    for(int i=0 ; i<6 ; i++ )
    {
        // lampadaire 1 + sa lumière
        ObjetGl* Lamp = new ObjetGl( "resources/model/Lamp/Lamp.obj",_objManager.mShaders["shaderObj"]);
        Lamp->mPosition0= glm::vec3(-100+i*30, 0.0f, 5);
        Lamp->mID="LampU"+std::to_string (i);
        _objManager.Add(Lamp);

        ObjetGl* LampLight = new ObjetGl( "resources/model/cube.obj",_objManager.mShaders["shaderBLight"]);
        LampLight->mPosition0 = Lamp->mPosition0+glm::vec3(0,3.2,0);
        LampLight->mColor = glm:: vec3 (1.0f, 1.0f, 0.0f);
        LampLight->mScale0=  glm::vec3(0.1);
        LampLight->mCallback = callback_lampadaire;
        LampLight->mID = "LampLightU"+std::to_string (i);
        LampLight->SetLightProporties(0,               //type de lumière
                                      glm::vec3(0),   // offset
                                      glm::vec3(0.),   // direction
                                      0.2,              // diffuse
                                      glm::vec3(0.5f), // ambient
                                      glm::vec3(1)); // specular
        _objManager.AddLight (LampLight);


        // lampadaire 2 + sa lumière
        ObjetGl* Lamp2 = new ObjetGl( "resources/model/Lamp/Lamp.obj",_objManager.mShaders["shaderObj"]);
        Lamp2->mPosition0= glm::vec3(-100+i*30, 0.0f, 12);
        Lamp2->mID="LampD"+std::to_string (i);
        _objManager.Add(Lamp2);

        ObjetGl* LampLight2 = new ObjetGl( "resources/model/cube.obj",_objManager.mShaders["shaderBLight"]);
        LampLight2->mPosition0 = Lamp2->mPosition0+glm::vec3(0,3.2,0);
        LampLight2->mColor = glm:: vec3 (1.0f, 1.0f, 0.0f);
        LampLight2->mScale0=  glm::vec3(0.1);
        LampLight2->mCallback = callback_lampadaire;
        LampLight2->mID = "LampLightD"+std::to_string (i);
        LampLight2->SetLightProporties(0,               //type de lumière
                                      glm::vec3(0),   // offset
                                      glm::vec3(0.),   // direction
                                      0.2,              // diffuse
                                      glm::vec3(0.5f), // ambient
                                      glm::vec3(1)); // specular
        _objManager.AddLight (LampLight2);
    }
}





void callback_lampadaire(ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets)
{
    _currentObj.mEstAllume = (_view.mCouleurCiel<0.25f);
    //    std::cout << _currentObj.mID << " est =" <<  _currentObj.mEstAllume << " et sin=" << _currentObj.mView->mCouleurCiel << std::endl;
}
