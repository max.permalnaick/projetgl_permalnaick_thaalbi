#include "objetglmanager.h"
#include "view.h"


void LoadObjHouse(ObjetGlManager& _objManager)
{
    ObjetGl* House = new ObjetGl("resources/model/House/house.obj",_objManager.mShaders["shaderObj"]);
    House->mPosition0=glm::vec3(-10,0,0);
    House->mRotations0.push_back (std_pair_rotation( (-M_PI)/2 , glm::vec3(1.0f, 0.0f, 0.0f) ) );
    House->mScale0= ( glm::vec3(0.1f));
    House->mID="House";
    _objManager.Add(House);

    ObjetGl* House2 = new ObjetGl(House);
    House2->mPosition0 = (glm::vec3(10,0,20));
    House2->mRotations0.push_back (std_pair_rotation( (-M_PI)/2 , glm::vec3(1.0f, 0.0f, 0.0f) ) );
    House2->mScale0= ( glm::vec3(0.1f));
    House2->mID="House2";
    _objManager.Add(House2);



}

