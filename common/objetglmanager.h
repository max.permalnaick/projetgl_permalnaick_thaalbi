#ifndef OBJETGLMANAGER___
#define OBJETGLMANAGER___

#include "objetgl.h"

/**
 * @brief The ObjetGlManager class
 */
class ObjetGlManager
{
public:
    ObjetGlManager() { }
    ~ObjetGlManager()
    {
        this->clear();
        std::map<std::string,Shader*>::iterator it = mShaders.begin ();
        for(it=mShaders.begin ();it!=mShaders.end ();it++)
        {
            Shader* ptr=it->second;
            delete ptr;
        }
    }
    void clear()
    {
        for(int i=0 ;i<mObjets.size() ; i++)
        {
            mObjets.at(i)->Delete ();
            delete mObjets.at(i);
        }
        mObjets.clear ();
        mObjetsmap.clear ();
        mSourcesLumieres.clear ();
        mCharacters.clear ();
        mBuildings.clear ();
    }
    void Add(ObjetGl* _obj)
    {
        mBuildings.push_back (_obj);
        mObjets.push_back (_obj);
        mObjetsmap[_obj->mID] = _obj;
    }
    void AddLight(ObjetGl* _obj)
    {
        mSourcesLumieres.push_back (_obj);
        mObjets.push_back (_obj);
        mObjetsmap[_obj->mID] = _obj;
    }
    void AddChar(ObjetGl* _obj)
    {
        mCharacters.push_back (_obj);
        mObjets.push_back (_obj);
        mObjetsmap[_obj->mID] = _obj;
    }
    void AddGround(ObjetGl* _obj)
    {
        mGrounds.push_back (_obj);
        mObjets.push_back (_obj);
        mObjetsmap[_obj->mID] = _obj;
    }
    ObjetGl* Get(std::string _ID)
    {
        if(mObjetsmap.find (_ID)!=mObjetsmap.end ())
            return mObjetsmap[_ID];
        return NULL;
    }
    std::map<std::string,Shader*> mShaders;
    std::vector<ObjetGl*> mSourcesLumieres;
    std::vector<ObjetGl*> mGrounds;
    std::vector<ObjetGl*> mCharacters;
    std::vector<ObjetGl*> mBuildings;
    std::vector<ObjetGl*> mObjets;
    std::map<std::string,ObjetGl*> mObjetsmap;
};
  
#endif

