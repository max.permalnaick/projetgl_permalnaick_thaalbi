#include "objetglmanager.h"
#include "view.h"


void LoadObjGround(ObjetGlManager& _objManager)
{
    // le sol
    std::vector<Vertex> vvertices = {
        /*     Positions                    |      Normales                |     UV     */
        {glm::vec3( 100.0f,  0.0f, 100.0f ),   glm::vec3(0.0f, 1.0f, 0.0f),  glm::vec2(500.0f,500.0f)}, // 1
        {glm::vec3( 100.0f, -0.0f,-100.0f ),   glm::vec3(0.0f, 1.0f, 0.0f),  glm::vec2(500.0f,  0.0f)}, // 2
        {glm::vec3(-100.0f, -0.0f,-100.0f ),   glm::vec3(0.0f, 1.0f, 0.0f),  glm::vec2(  0.0f,  0.0f)}, // 3
        {glm::vec3(-100.0f,  0.0f, 100.0f ),   glm::vec3(0.0f, 1.0f, 0.0f),  glm::vec2(  0.0f, 500.0f)},  // 4
    };
    std::vector<GLuint> vindices  = {0, 3, 1,
                                     3, 2, 1};
    ObjetGl* Sol = new ObjetGl(vvertices,
                               vindices,
                               "resources/texture/grass1.jpg",
                               _objManager.mShaders["shaderObj"]);
    Sol->mPosition0 = glm::vec3(0, 0.0f, 0);
    Sol->mScale0    = glm::vec3(1.0f);
    Sol->mID        = "Sol";
    _objManager.AddGround(Sol);

    // la route
    for(int i=0; i<40 ; i++)
    {
        ObjetGl* Road = new ObjetGl("resources/model/road/road.obj",_objManager.mShaders["shaderObj"]);
        Road->mPosition0 = glm::vec3(-100.0f+i*5, -0.1f, 9.0f);
        Road->mRotations0.push_back ( std_pair_rotation((GLfloat)M_PI_2, glm::vec3(0.f, 1, 0)) );
        Road->mID = "Road"+std::to_string (i);
        _objManager.AddGround(Road);
    }
}
