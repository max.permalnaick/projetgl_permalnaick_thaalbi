#ifndef OBJETGL__
#define OBJETGL__

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>

#include "Model.h"
#include "view.h"


class ObjetGlManager;
typedef std::pair<GLfloat,glm::vec3> std_pair_rotation;

/**
 * @brief gère les objets géométriques opengl. Les objets sont chargés avec la lib assimp.
 *        Il est possible de dessiner et d'appliquer les échanges avec le shader via les méthodes Update(...) et draw().
 *        Pour appliquer un transformation spéciale à l'objet, il faut renseigner mCallback qui sera appelée automatiquement dans Update(...)
 */
class ObjetGl
{
public:
    /**
     * @brief pour identifier l'objet
     */
    std::string mID;

    /**
     * @brief Positionnement intial de l'objet dans le monde
     */
    glm::vec3 mPosition0;
    std::vector<std_pair_rotation> mRotations0;
    glm::vec3 mScale0;
    glm::vec3 mColor;

    /**
     * @brief les variables d'échange avec le shader pour la gestion de la lumière
     */
    glm::vec3 mLightAmbientStrength;
    glm::vec3 mLightSpecuiar;
    glm::vec3 mLightOffset;
    float mDiffuseStrength;
    std::string mLightType; //dirLight,pointLights,spotLight
    void SetLightProporties(int _LightType=0,
                            glm::vec3 _Lightoffset=glm::vec3(0.f),
                            glm::vec3 _LightDirection=glm::vec3(0.f),
                            float _LightDiffuse = ( 0.5f),
                            glm::vec3 _LightAmbient=glm::vec3( 0.2f),
                            glm::vec3 _LightSpecuiar= glm::vec3( 1.0f),
                            float _LightConstant=1.,
                            float _LightLinear=0.09,
                            float _LightQuadratic=0.032,
                            float _LightCutoff=glm::cos(glm::radians(12.5f)),
                            float _LightOuterCutoff=glm::cos(glm::radians(15.0f)))
    {
        mLightOffset = _Lightoffset;
        mLightAmbientStrength = _LightAmbient;
        mDiffuseStrength = _LightDiffuse;
        mLightSpecuiar = _LightSpecuiar;
//        mLightDirection=_LightDirection;
//        mLightDiffuse=_LightDiffuse;
//        mLightConstant=_LightConstant;
//        mLightLinear=_LightLinear;
//        mLightQuadratic=_LightQuadratic;
//        mLightCutoff=_LightCutoff;
//        mLightOuterCutoff=_LightOuterCutoff;
        switch(_LightType)
        {
        case 0:
            mLightType = "dirLight";
            break;
        case 1:
            mLightType = "pointLights";
            break;
        default:
            mLightType = "spotLight";
            break;
        }
    }

    /**
     * @brief Création du model OpenGL avec Assimp
     */
    const ObjetGl* mRef;
    std::string mFileObj;
    Model* mModelObj_p;
    Shader* mShader_p;

    /**
     * @brief Gestion du déplacement de l'objet dans le monde
     */
    void (*mCallback) (ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets);//Pointeur de fonction pour gérer le déplacement
    glm::vec3 mTranslation;
    std_pair_rotation mRotation;

    /**
     * @brief les variables spéciales pour le soleil et la lampe
     */
    bool  mEstAllume;
    float mRad;
    float mRayon;
    std::map<std::string,glm::vec3> mUserVars_str; // la méthode des variables multiples et polymorphes
    std::map<std::string,double> mUserVars_float; // la méthode des variables multiples et polymorphes

    /**
     * @brief instancie avec un fichier 3D et un shader associé
     * @param _fileObj
     * @param _shader
     */
    ObjetGl(std::string _fileObj,Shader* _shader)
        : mFileObj(_fileObj),mDiffuseStrength(0.1), mEstAllume(false), mScale0(glm::vec3(1.f)),mCallback(NULL),mRef(NULL),mShader_p(_shader)
    {
        mModelObj_p = new Model(mFileObj); //throw exception
        SetLightProporties();
    }

    /**
     * @brief instancie avec des vertices, indices, son fichier texture et un shader associé
     * @param _vertices
     * @param _indices
     * @param _pathTexture
     * @param _shader
     */
    ObjetGl(vector<Vertex> _vertices, vector<GLuint> _indices, string _pathTexture,Shader* _shader)
        : mFileObj(""),mDiffuseStrength(0.1), mEstAllume(false), mScale0(glm::vec3(1.f)),mCallback(NULL),mRef(NULL),mShader_p(_shader)
    {
        mModelObj_p = new Model(_vertices,_indices,_pathTexture); //throw exception
        SetLightProporties();
    }

    /**
     * @brief instancie avec un autre objet gl en référence : évite la duplication de texture en vram
     * @param _ref
     */
    ObjetGl( ObjetGl* _ref)
        : mFileObj(""),mDiffuseStrength(0.1), mEstAllume(false), mScale0(glm::vec3(1.f)),mCallback(NULL)
    {
        if(!_ref)
            throw std::string("Objet de référence doit être non nul");

        mRef=_ref;
        mModelObj_p=mRef->mModelObj_p;
        mShader_p = mRef->mShader_p;
        if(!mShader_p)
            throw std::string("Pas de shader associé à l'objet");
        SetLightProporties();
    }

    ~ObjetGl()
    {
        Delete ();
    }

    /**
     * @brief GetCurrPosition
     * @return
     */
    glm::vec3 GetCurrPosition()
    {
        return mPosition0+mTranslation;
    }

    /**
     * @brief libère les ressources
     */
    void Delete()
    {
        if(!mRef)
        {
            if(mModelObj_p) delete mModelObj_p;
            mModelObj_p=NULL;
        }
    }

    /**
     * @brief Afficher l'objet avec ses transformations.
     */
    void Draw()
    {
        mModelObj_p->Draw (*mShader_p);
    }

    /**
     * @brief applique les effets de la lumière sur un objet
     * @param _lighter
     */
    void ApplyLight( ObjetGl& _lighter, int _indexLight )
    {
        glm::vec3 lightPos = _lighter.mTranslation+_lighter.mPosition0+_lighter.mLightOffset;
        glm::vec3 lightDiffuse = _lighter.mColor * glm::vec3(_lighter.mDiffuseStrength);
        glm::vec3 lightAmbient = lightDiffuse * _lighter.mLightAmbientStrength;
        std::string fct = "t_light";

        mShader_p->use ();
        mShader_p->setInt (fct+"["+std::to_string (_indexLight)+"].enable"  , _lighter.mEstAllume );
        mShader_p->setVec3 ("color"  , _lighter.mColor );

        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].position", lightPos);
        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].ambient" , lightAmbient);
        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].diffuse" , lightDiffuse); // darken the light a bit to fit the scene
        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].specular", _lighter.mLightSpecuiar);

//        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].direction", camera.Front);
//        mShader_p->setFloat(fct+"["+std::to_string (_indexLight)+"].cutOff", glm::cos(glm::radians(12.5f)));
//        mShader_p->setFloat(fct+"["+std::to_string (_indexLight)+"].outerCutOff", glm::cos(glm::radians(17.5f)));

//        // light properties
//        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].ambient", 0.1f, 0.1f, 0.1f);
//        // we configure the diffuse intensity slightly higher; the right lighting conditions differ with each lighting method and environment.
//        // each environment and lighting type requires some tweaking to get the best out of your environment.
//        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].diffuse", 0.8f, 0.8f, 0.8f);
//        mShader_p->setVec3(fct+"["+std::to_string (_indexLight)+"].specular", 1.0f, 1.0f, 1.0f);
//        mShader_p->setFloat(fct+"["+std::to_string (_indexLight)+"].constant", 1.0f);
//        mShader_p->setFloat(fct+"["+std::to_string (_indexLight)+"].linear", 0.09f);
//        mShader_p->setFloat(fct+"["+std::to_string (_indexLight)+"].quadratic", 0.032f);


//        std::cout << "lum pos " << lightPos.x << " " << lightPos.y << " " << lightPos.z << std::endl;
//        std::cout << "lum diffuse " << lightDiffuse.x << " " << lightDiffuse.y << " " << lightDiffuse.z << std::endl;
//        std::cout << "lum ambient " << lightAmbient.x << " " << lightAmbient.y << " " << lightAmbient.z << std::endl;
    }

    /**
     * @brief Met à jour les déplacements de l'objet dans le monde et applique la fenêtre de caméra
     * @param _view
     * @param _time
     */
    void Update(ViewManager& _view, float& _time, std::vector<ObjetGl*>& _objets)
    {
        if(mCallback)
        {
            mCallback(*this, _view, _time, _objets);
        }

        //la vue
        mShader_p->use ();
        mShader_p->setMat4 ("projection", _view.mProjectionMvc  );
        mShader_p->setMat4 ("view"      , _view.mViewMvc        );
        mShader_p->setVec3 ("viewPos"   , _view.mCamera.Position);

        //Positionnement initiale de l'objet dans le monde
        glm::mat4 modelMvc = glm::mat4(1.0f);
        modelMvc = glm::translate(modelMvc, mPosition0);
        for(int i=0 ; i<mRotations0.size () ; i++)
            modelMvc = glm::rotate(modelMvc, mRotations0[i].first, mRotations0[i].second);

        //Transformations appliquées sur l'objet dans le monde
        modelMvc = glm::translate(modelMvc, mTranslation);
        if(mRotation.second!=glm::vec3(0.f))
            modelMvc = glm::rotate(modelMvc, mRotation.first,mRotation.second);
        modelMvc = glm::scale(modelMvc, mScale0);
        mShader_p->setMat4 ("model", modelMvc);

        //caractéristiques objet
//        mShader_p->setVec3 ("material.ambient"  , glm::vec3(1.0f, 1.0f, 1.0f));
//        mShader_p->setVec3 ("material.diffuse"  , glm::vec3(1.0f, 1.0f ,1.0f));
//        mShader_p->setVec3 ("material.specular" , glm::vec3(0.2f, 0.2f, 0.2f));
        mShader_p->setFloat("material.shininess", 16.0f);
    }

    glm::vec3 mBaseDownRight;
    glm::vec3 mBaseTopLeft;
    glm::vec3 mUpDownRight;
    glm::vec3 mUpTopLeft;
    void CreateArea()
    {
        mBaseDownRight=glm::vec3(0);
        mBaseTopLeft=glm::vec3(0);
        mUpDownRight=glm::vec3(0);
        mUpTopLeft=glm::vec3(0);
        for(int i=0 ; i<mModelObj_p->mMeshes.size () ; i++)
        {
            for(int v=0 ; v<mModelObj_p->mMeshes.at(i).mVertices.size () ; v++)
            {
//                if()
            }
        }
    }

    /**
     * @brief récupère la valeur de la variable spéciale identifiée par _name
     * @param _name
     * @param _value
     * @return vrai si la valeur existe et renseigne _value sinon faux
     */
    bool GetVar(const std::string& _name, glm::vec3& _value)
    {
        if(mUserVars_str.find(_name)==mUserVars_str.end ())
        {
            return false;
        }
        _value = mUserVars_str[_name];
        return true;
    }
    bool GetVar(const std::string& _name, double& _value)
    {
        if(mUserVars_float.find(_name)==mUserVars_float.end ())
        {
            return false;
        }
        _value = mUserVars_float[_name];
        return true;
    }

    /**
     * @brief fixe une variable spéciale identifiée par _name avec la valeur _value
     */
    void SetVar(const std::string& _name, const glm::vec3& _value)
    {
        mUserVars_str[_name] = _value;
    }
    void SetVar(const std::string& _name, double _value)
    {
        mUserVars_float[_name] = _value;
    }
};

#endif

