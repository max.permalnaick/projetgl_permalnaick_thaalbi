#include "objetglmanager.h"
#include "view.h"


void callback_deplacement (ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets);

void LoadObjBoy(ObjetGlManager& _objManager)
{
    ObjetGl* Boy = new ObjetGl("resources/model/Boy/Boy.obj",_objManager.mShaders["shaderObj"]);
    Boy->mPosition0= glm::vec3(-7.f, 0.0f, 15.0f);
    Boy->mScale0 = glm::vec3(1/100.f);
    Boy->mCallback = callback_deplacement;
    Boy->mID="Boy";
    _objManager.AddChar(Boy);
}





#include <random>
#include <algorithm>
//static std::default_random_engine generator;
static std::mt19937 generator;

void callback_deplacement (ObjetGl& _currentObj, ViewManager& _view, float& _timer, std::vector<ObjetGl*>& _objets)
{
    glm::vec3 lastPos;
    double target;
    std::uniform_int_distribution<int> distribution(0,_objets.size ()-1);

    // pas d'objets à suivre
    if(!_objets.size ())
        return;

    // temps écoulé depuis la dernière loop
    double lastTime=0.;
    _currentObj.GetVar ("lastTime",lastTime);
    lastTime = _timer - lastTime;
    if(lastTime<0.2)
        return;
    _currentObj.SetVar ("lastTime",_timer);

    // récupère la position de suzanna et la cible
    if(!_currentObj.GetVar ("target",target))
    {
        _currentObj.SetVar ("target",0);
        target=0;
    }
    if(!_currentObj.GetVar ("lastPos",lastPos))
    {
        _currentObj.SetVar ("lastPos",_currentObj.GetCurrPosition ());
        lastPos=_currentObj.GetCurrPosition ();
    }

    // position de la cible
    glm::vec3 pos = _objets.at(target)->GetCurrPosition ();
    glm::vec3 dir ;

    // vérification de la distance, passe à l'objet suvant si ok
    double diffDist =  glm::distance(lastPos,pos);
    if( diffDist <= 1.8f)
    {
        target = distribution(generator);
        _currentObj.SetVar ("target",target);
        return;
    }

    // sion dépalcment
    else
    {
        dir = 1.f*glm::normalize(pos-lastPos); dir.y=0;
        double angle = (dir.x!=0)?atan (dir.z/dir.x) : 0. ;
        _currentObj.mRotation = std_pair_rotation(angle-M_PI_2, glm::vec3(0.0f, 1.0f, 0.0f));
        _currentObj.mTranslation +=  dir;
        _currentObj.SetVar ("lastPos",_currentObj.GetCurrPosition ());
    }

    std::cout << _currentObj.mID << ".target="<<_objets.at(target)->mID  << std::endl;
    std::cout << _objets.at(target)->mID << ".position="  << pos.x << " " << pos.z << std::endl;
    std::cout << _currentObj.mID << ".dist(target)="<< diffDist  << std::endl;
    std::cout << _currentObj.mID << ".position="<<_currentObj.GetCurrPosition().x << " " <<_currentObj.GetCurrPosition().z  << std::endl;
    std::cout << _currentObj.mID << ".rotation="<<_currentObj.mRotation.first << std::endl;
    std::cout << _currentObj.mID << ".dir="<<dir.x << " " << dir.z << " z/x=" << dir.z/dir.x << " tan(z/x)=" << tan(dir.z/dir.x)<< " atan=" << atan(dir.z/dir.x) << std::endl;
}


