/**
  */

#include "objetglmanager.h"
#include "view.h"
#include <random>
#include <algorithm>



bool Intersect(glm::vec3 _obj[2], glm::vec3 _rect[2]);
void LoadObjTree(ObjetGlManager& _objManager)
{
    // arbres fixes
    ObjetGl* Tree1 = new ObjetGl( "resources/model/Trees/Tree1/Tree1.3ds",_objManager.mShaders["shaderObj"]);
    Tree1->mPosition0=   ( glm::vec3(10.0f, 0.0f, -8.0f))/2.f;
    Tree1->mRotations0.push_back (std_pair_rotation( (-M_PI)/2 , glm::vec3(1.0f, 0.0f, 0.0f) ) );
    Tree1->mID="TreeSapin1";
    Tree1->mScale0 = glm::vec3(2);
    _objManager.Add(Tree1);

    ObjetGl* Tree2 = new ObjetGl( "resources/model/Trees/Tree2/Tree2.3ds",_objManager.mShaders["shaderObj"]);
    Tree2->mPosition0=   (glm::vec3(-5.0f, 0.0f, 10.0f))/0.5f;
    Tree2->mRotations0.push_back (std_pair_rotation( (-M_PI)/2 , glm::vec3(1.0f, 0.0f, 0.0f) ) );
    Tree2->mScale0 = glm::vec3(0.5);
    Tree2->mID="TreeAutre1";
    _objManager.Add(Tree2);


    // zone autorisée
    std::mt19937 generator;
    glm::vec3 grandRectangle[2] = {glm::vec3(100,100,0),glm::vec3(-100,-100,0)};
    glm::vec3 rectangleMaisons[2] = {glm::vec3(30,40,0),glm::vec3(-30,-20,0)};
    glm::vec3 rectangleRoute[2] = {glm::vec3(100,22,0),glm::vec3(-100,-5,0)};
    for(int arbre=0 ; arbre < 40 ; arbre++)
    {
        ObjetGl* Tree = new ObjetGl(Tree2);
        std::uniform_int_distribution<int> distributionX(-35,35);
        std::uniform_int_distribution<int> distributionZ(-35,35);
        bool bOk=false;
        while(!bOk)
        {
            Tree->mPosition0=  ( glm::vec3(distributionX(generator), 0.0f, distributionZ(generator)) )/0.5f;
            glm::vec3 rectArbre[2]={Tree->mPosition0,Tree->mPosition0+glm::vec3(2,0,2)};

            if(Intersect(rectArbre,grandRectangle))
                continue;
            if(Intersect(rectArbre,rectangleMaisons))
                continue;
            if(Intersect(rectArbre,rectangleRoute))
                continue;

            bOk=true;
        }

        Tree->mRotations0.push_back (std_pair_rotation( (-M_PI)/2 , glm::vec3(1.0f, 0.0f, 0.0f) ) );
        Tree->mID="TreeRand"+std::to_string (arbre);
        Tree->mScale0 = glm::vec3(0.5);
        _objManager.Add(Tree);
    }
}


bool Intersect(glm::vec3 _obj[2], glm::vec3 _otherArea[2])
{
    bool lowerB_isInside = (_otherArea[0].x >= _obj[0].x && _otherArea[1].x >= _obj[0].y) &&
                           (_otherArea[0].x <  _obj[1].x && _otherArea[0].y <  _obj[1].y);

    bool upperB_isInside = (_otherArea[1].x >  _obj[0].x && _otherArea[1].y >  _obj[0].y) &&
                           (_otherArea[1].x <= _obj[1].x && _otherArea[1].y <= _obj[1].y);

    return (lowerB_isInside && upperB_isInside) ;
}
