#include "objetglmanager.h"
#include "view.h"


void LoadObjCar(ObjetGlManager& _objManager)
{
    ObjetGl* Car = new ObjetGl("resources/model/toon_car/toon_car.obj",_objManager.mShaders["shaderLight"]);
    Car->mPosition0=   (glm::vec3(0));
    Car->mRotation = std_pair_rotation(-M_PI_2,glm::vec3(1,0,0));
    Car->mScale0 = glm::vec3(0.05);
    Car->mID="Car";
    _objManager.Add(Car);
}
