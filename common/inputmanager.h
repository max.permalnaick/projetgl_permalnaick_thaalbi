#ifndef INPUTMANAGER_H__
#define INPUTMANAGER_H__

#include "view.h"

/**
 * @brief The ObjetGl class
 */
class InputManager
{
public:
    /**
     * @brief InputManager
     * @param _view
     */
    InputManager(ViewManager& _view_) : mView(_view_)
    {
        mLastX = 0.;
        mLastY = 0.;
        mIsFirstMouse = true;
    }

    /**
     * @brief PollEvents
     */
    void PollEvents()
    {
        glfwPollEvents();

        // quit app?
        if(glfwGetKey( mView.mWindow, GLFW_KEY_ESCAPE ) == GLFW_PRESS)
            glfwSetWindowShouldClose(mView.mWindow, GL_TRUE);

        // camera movements
        GLfloat currentFrame = glfwGetTime();
        mView.mCamera.deltaTime = currentFrame - mView.mCamera.lastFrame;
        mView.mCamera.lastFrame = currentFrame;

        _computeInputs_processKeyboard();
        _computeInputs_processMouse();
    }

    bool IsKeyPressed(int _glfw_key)
    {
        return (glfwGetKey( mView.mWindow, _glfw_key ) == GLFW_PRESS);
    }

    bool IsKeyReleased(int _glfw_key)
    {
        return (glfwGetKey( mView.mWindow, _glfw_key ) == GLFW_RELEASE);
    }

private:
    /**
     * @brief mLastX
     */
    GLfloat mLastX ;
    GLfloat mLastY ;
    bool mIsFirstMouse;
    ViewManager& mView;

    /**
     * @brief _computeInputs_processKeyboard
     */
    void _computeInputs_processKeyboard()
    {
        // Camera controls
        if (glfwGetKey( mView.mWindow, GLFW_KEY_W ) == GLFW_PRESS)
        {
            mView.mCamera.ProcessKeyboard(Camera::FORWARD, mView.mCamera.deltaTime);
        }
        if (glfwGetKey( mView.mWindow, GLFW_KEY_S ) == GLFW_PRESS)
        {
            mView.mCamera.ProcessKeyboard(Camera::BACKWARD, mView.mCamera.deltaTime);
        }
        if (glfwGetKey( mView.mWindow, GLFW_KEY_A ) == GLFW_PRESS)
        {
            mView.mCamera.ProcessKeyboard(Camera::LEFT, mView.mCamera.deltaTime);
        }
        if (glfwGetKey( mView.mWindow, GLFW_KEY_D ) == GLFW_PRESS)
        {
            mView.mCamera.ProcessKeyboard(Camera::RIGHT, mView.mCamera.deltaTime);
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_ENTER ) == GLFW_PRESS)
        {
            mView.mCamera.Pitch=Camera::PITCH;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_1 ) == GLFW_PRESS)
        {
            mView.mCamera.Position = glm::vec3(0.f,(double)Camera::EYE,300.);
            mView.mCamera.Pitch=Camera::PITCH;
            mView.mCamera.Yaw = Camera::YAW;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_2 ) == GLFW_PRESS)
        {
            mView.mCamera.Position = glm::vec3(0.f,(double)Camera::EYE,30.);
            mView.mCamera.Pitch=Camera::PITCH;
            mView.mCamera.Yaw = Camera::YAW;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_3 ) == GLFW_PRESS)
        {
            mView.mCamera.Position = glm::vec3(0.f,50.,0.);
            mView.mCamera.Pitch=Camera::YAW;
            mView.mCamera.Yaw = Camera::YAW;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_KP_8 ) == GLFW_PRESS)
        {
            mView.mCamera.Position += glm::vec3(0.f,1.,0);
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_KP_5 ) == GLFW_PRESS)
        {
            mView.mCamera.Position += glm::vec3(0.f,-1.,0);
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_KP_4 ) == GLFW_PRESS)
        {
            mView.mCamera.Yaw-=1;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_KP_6 ) == GLFW_PRESS)
        {
            mView.mCamera.Yaw+=1;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_KP_7 ) == GLFW_PRESS)
        {
            mView.mCamera.Pitch+=1;
            mView.mCamera.updateCameraVectors ();
        }
        if(glfwGetKey( mView.mWindow, GLFW_KEY_KP_9 ) == GLFW_PRESS)
        {
            mView.mCamera.Pitch-=1;
            mView.mCamera.updateCameraVectors ();
        }
    }

    /**
     * @brief _computeInputs_processMouse
     */
    void _computeInputs_processMouse()
    {
        double xpos, ypos;
        glfwGetCursorPos (mView.mWindow,&xpos,&ypos);
        if( glfwGetMouseButton(mView.mWindow,GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)//ADD
        {
            if(mIsFirstMouse)
            {
                glfwGetCursorPos (mView.mWindow,&xpos,&ypos);
                mLastX = xpos;
                mLastY = ypos;
                mIsFirstMouse = false;
            }

            GLfloat xoffset= (xpos - mLastX)/5;
            GLfloat yoffset= (mLastY - ypos)/20;
            mView.mCamera.ProcessMouseMovement(xoffset, yoffset);

            mLastX = xpos;
            mLastY = ypos;
        }
        else
        {
            if(!mIsFirstMouse)
                mIsFirstMouse=true;
        }
    }
};


#endif

