//compilation options
//cp
//"%{CurrentProject:Path}/resources" ./ -r

#include "defs.h"

/**
 * Le programme est structuré avec plusieurs modules/classes.
 * Un module ViewManager qui instancie la fenêtre de contexte pour openGL via GLFW.
 */
int main(int _argc, char* _argv[])
{
    try
    {
        ViewManager view(800,600,"Projet opengl");
        InputManager input(view);
        ObjetGlManager objManager;
        _chargeObjetsGL(objManager);
        _mainLoop(view,input,objManager);
    }
    catch(std::string &s)
    {
        std::cout << "Exception : " << s << std::endl;
        return 1;
    }
    catch(std::exception &e)
    {
        std::cout << "Exception : " << e.what ()<< std::endl;
        return 1;
    }

    return 0;
}



static int debugLevel=0;
void SetVerbose(int _verboseLevel) {debugLevel = _verboseLevel;}
void Debug(std::stringstream _val, int _verboseLevel=0) { if(debugLevel>=_verboseLevel) std::cout << _val.str() << std::endl; }
void Log(std::stringstream _val) { std::cout << _val.str() << std::endl; }
