*Architecture du code:

-InputManager class:  gére le clavier et contient les gestions de base de la camera et sa rotation(Yaw et Pich et pas le Roll).

-Model class: nous avons ajouté "TextureFromFile" qui était une fonction externe, intégrée dans la classe pour éviter les définitions multiples de la fonction.Le fichier "model.h" est appelé dans plusieurs autres fichiers(astuce pour éviter de créer un .cpp inutile).

-ObjetGl class: gère les objets géométriques opengl. Les objets sont chargés avec la lib assimp.
	     Il est possible de dessiner et d'appliquer les échanges avec le shader via les méthodes Update(...) et draw(). Pour appliquer une transformation spéciale à l'objet, il faut renseigner mCallback qui sera appelée automatiquement dans Update(...).

-ObjetGlManager class: gére les objets et leurs suppression (gérer la mémoire) donc ce n'est pas nécessaire de libérer les objets alloués dynamiquement. La méthode utilisée ici n'est pas entiérement <<MemorySafe>> afin de simplifier le développement. Alors que la méthode "Safe" stipule la création de "ObjetGl" via une méthode de ObjetGlManager.

-ViewManager class: instancie la fenêtre de context opengl via la biblio glfw. 
	     Aussi, elle gére le cycle jour et nuit. Elle a un paramètre camera.
 	     Elle gère toute la projection du context opengl (via glm).
	     Ses attributs de projection vont étre appliqués sur tout les objets à afficher.


*Les modules que nous avons implémentés sont:

Nous avons développé:

-Une lumière dynamique: Dans notre scéne nous avons un soleil qui décrit un cercle autour de notre 		     base. Nous avons également ajouté la couleur dégradée du ciel (class ViewManager et Soleil::mCallback()).
-Plusieurs lumières statiques: Dans notre scéne nous avons des lampadaires qui s'allument une fois la nuit est tombée (lamp::mCallback()) le long de la route : nous avons également pris en compte le nombre de lumière dans les shaders pour le multi-lightning.
-Dépacement planifié: Dans notre scéne nous avons un personnage qui se déplace d'objet en objet (ObjetGl)(Boy::mCallback()) à l'aide d'un random. Il est possible également de suivre le personnage en appuyant sur la touche "f" du clavier(InputManager).
-Le Random de la  position des arbres dans la scène.


*Le déplacement dans le monde
** clavier
Les touches ZSQD pour les déplacements en latéral de la caméra,
Les touches & (en US touche 1) pour un niveau de zoom éloigné de la scène 
	    é (en US touche 2) pour un niveau de zoom proche de la scène
	    " (en US touche 3) pour un niveau de zoom avec une vue de dessus de la scène
Les touches 5 et 8 du PVN, pour les translations haut et bas de la caméra
Les touches 4 et 6 du PVN, pour les YAW de la caméra
Les touches 7 et 9 du PVN, pour les PITCH de la caméra
** souris
Pour utiliser la rotation avec la souris, il faut bouger la souris en maintenant l'appui sur le bouton gauche de la souris.


*Comment utiliser le code:

Pour utiliser le code, il faut disposer ces bibliothéques:

-Assimp : Pour le chargement des objets 3D et la gestion de l'affichage de ces derniers via l'OpenGL.
-glm : Pour les opérations mathématiques liées à l'OpenGL.
-glew : Le driver OpenGL.
-glfw : Pour la gestion du context, fenêtrage et périphériques(souris, clavier).
-SOIL : Pour le chargement des images (les textures).!!!!!!ATTENTION:SOIL a été rajouté dans le CmakeLists.txt 

ATTENTION : Il faut copier les ressources dans la racine de l'exécutable.

Merci :)
