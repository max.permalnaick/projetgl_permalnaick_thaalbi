#ifndef defs_h___
#define defs_h___

// Std. Includes
#include <string>
#include <iostream>
#include <stdlib.h>

// GLEW
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GLM Mathemtics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// nos classes
#include "objetglmanager.h"
#include "shader.h"
#include "Camera.h"
#include "Model.h"
#include "view.h"
#include "inputmanager.h"

void _chargeObjetsGL ( ObjetGlManager& _objManager );
void _mainLoop ( ViewManager& _view, InputManager& _input, ObjetGlManager& _objManager );


#endif
