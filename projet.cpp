//#include <execinfo.h>
//#include <signal.h>
//#include <string.h>

//#include <iostream>
//#include <cstdlib>
//#include <stdexcept>

//void my_terminate(void);

//namespace {
//    // invoke set_terminate as part of global constant initialization
//    static const bool SET_TERMINATE = std::set_terminate(my_terminate);
//}

//// This structure mirrors the one found in /usr/include/asm/ucontext.h
//typedef struct _sig_ucontext {
//   unsigned long     uc_flags;
//   struct ucontext   *uc_link;
//   stack_t           uc_stack;
//   struct sigcontext uc_mcontext;
//   sigset_t          uc_sigmask;
//} sig_ucontext_t;

//void crit_err_hdlr(int sig_num, siginfo_t * info, void * ucontext) {
//    sig_ucontext_t * uc = (sig_ucontext_t *)ucontext;

//    // Get the address at the time the signal was raised from the EIP (x86)
//    void * caller_address = (void *) uc->uc_mcontext.rip;

//    std::cerr << "signal " << sig_num
//              << " (" << strsignal(sig_num) << "), address is "
//              << info->si_addr << " from "
//              << caller_address << std::endl;

//    void * array[50];
//    int size = backtrace(array, 50);

//    std::cerr << __FUNCTION__ << " backtrace returned "
//              << size << " frames\n\n";

//    // overwrite sigaction with caller's address
//    array[1] = caller_address;

//    char ** messages = backtrace_symbols(array, size);

//    // skip first stack frame (points here)
//    for (int i = 1; i < size && messages != NULL; ++i) {
//        std::cerr << "[bt]: (" << i << ") " << messages[i] << std::endl;
//    }
//    std::cerr << std::endl;

//    free(messages);

//    exit(EXIT_FAILURE);
//}

//void my_terminate() {
//    static bool tried_throw = false;

//    try {
//        // try once to re-throw currently active exception
//        if (!tried_throw++) throw;
//    }
//    catch (const std::exception &e) {
//        std::cerr << __FUNCTION__ << " caught unhandled exception. what(): "
//                  << e.what() << std::endl;
//    }
//    catch (...) {
//        std::cerr << __FUNCTION__ << " caught unknown/unhandled exception."
//                  << std::endl;
//    }

//    void * array[50];
//    int size = backtrace(array, 50);

//    std::cerr << __FUNCTION__ << " backtrace returned "
//              << size << " frames\n\n";

//    char ** messages = backtrace_symbols(array, size);

//    for (int i = 0; i < size && messages != NULL; ++i) {
//        std::cerr << "[bt]: (" << i << ") " << messages[i] << std::endl;
//    }
//    std::cerr << std::endl;

//    free(messages);

//    abort();
//}

/// in main()
//    struct sigaction sigact;
//    sigact.sa_sigaction = crit_err_hdlr;
//    sigact.sa_flags = SA_RESTART | SA_SIGINFO;
//    if (sigaction(SIGABRT, &sigact, (struct sigaction *)NULL) != 0) {
//        std::cerr << "error setting handler for signal " << SIGABRT
//                  << " (" << strsignal(SIGABRT) << ")\n";
//        exit(EXIT_FAILURE);
//    }


//// Std. Includes
//#include <string>

//// GLEW
//#include <GL/glew.h>

//// GLFW
//#include <GLFW/glfw3.h>

//// GL includes
//#include "Shader.h"
//#include "Camera.h"
//#include "Model.h"

//// GLM Mathemtics
//#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

//// Other Libs
//#include <SOIL/SOIL.h>

//// Properties
//GLuint screenWidth = 800, screenHeight = 600;

//int main()
//{
//    // Init GLFW
//    glfwInit();
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
//    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
//    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
//    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
//    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

//    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "OpenGL", nullptr, nullptr); // Windowed
//    glfwMakeContextCurrent(window);

//    glewExperimental = GL_TRUE;
//    glewInit();

//    // Define the viewport dimensions
//    int width, height;
//    // On recupere les dimensions de la fenetre creee plus haut
//    glfwGetFramebufferSize(window, &width, &height);
//    glViewport(0, 0, width, height);

//    glEnable(GL_DEPTH_TEST);

//    Shader shader("../projetOpenGL/shaders/default.vertexshader", "../projetOpenGL/shaders/default.fragmentshader");

//    GLuint texture; // Declaration de l'identifiant

//    glGenTextures(1, &texture); // Generation de la texture
//    // On bind la texture cree dans le contexte global d'OpenGL
//    glBindTexture(GL_TEXTURE_2D, texture);
//    // Modification des parametres de la texture
//    // Methode de wrapping
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

//    // Methode de filtrage
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    // Chargement du fichier image en utilisant la lib SOIL
//    int twidth, theight;
//    string tex="../projetOpenGL/texture/soil.png";
//    unsigned char* data = SOIL_load_image(tex.c_str(), &twidth, &theight,0, SOIL_LOAD_RGB);
//    if(!data) std::cout<<"Enable to load texture : "<<tex<<std::endl;
//    // Association des donnees image a la texture
//    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight,0, GL_RGB, GL_UNSIGNED_BYTE, data);
//    // Generation de la mipmap
//    glGenerateMipmap(GL_TEXTURE_2D);
//    // On libere la memoire
//    SOIL_free_image_data(data);
//    // On unbind la texture
//    glBindTexture(GL_TEXTURE_2D, 0);

//    GLfloat vertices[] = {
//        /*     Positions    |      Normales     |     UV     */
//        50.0f,  0.0f, 50.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // Top Right
//        50.0f, -0.0f, -50.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Right
//        -50.0f, -0.0f, -50.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // Bottom Left
//        -50.0f,  0.0f, 50.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // Top Left
//    };

//    GLshort indices[]{
//        0, 1, 3,
//        1, 2, 3
//    };

//    GLuint VBO, VAO, EBO;

//    glGenVertexArrays(1, &VAO);
//    glGenBuffers(1, &VBO);
//    glGenBuffers(1, &EBO);
//    glBindVertexArray(VAO);

//    // On met notre EBO dans le contexte global
//    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
//    // On assigne au EBO le tableau d'indices
//    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
//    glBindBuffer(GL_ARRAY_BUFFER, VBO);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

//    // Attribut des positions
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
//    glEnableVertexAttribArray(0);
//    // Attribut des couleurs
//    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
//    glEnableVertexAttribArray(1);
//    // Attribut des normales
//    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)(6*sizeof(GLfloat)));
//    glEnableVertexAttribArray(2);

//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//    glBindVertexArray(0);

//    Model monModel("../projetOpenGL/model/tree.obj");

//    Camera camera(glm::vec3(0.0f, 5.0f, 20.0f), window);

//    // Game loop
//    while(!glfwWindowShouldClose(window))
//    {
//        glfwPollEvents();
//        camera.Do_Movement();

//        glClearColor(0.5f, 0.5f, 0.8f, 1.0f);
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

//        shader.Use();

//        glm::mat4 projection = glm::perspective(45.0f, (float)screenWidth/(float)screenHeight, 0.1f, 100.0f);
//        glm::mat4 view = camera.GetViewMatrix();

//        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
//        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));

//        //  Activiation  de la  texture
//        glActiveTexture(GL_TEXTURE0 );
//        //  Binding  de  notre  texture
//        glBindTexture(GL_TEXTURE_2D , texture );
//        //  Association  du  numero  de la  texture  pour le  shader
//        glUniform1i(glGetUniformLocation(shader.Program , "maTexture"), 0);
//        glBindVertexArray(VAO);
//        // On dessine l'objet courant
//        glDrawElements(GL_TRIANGLES, 2*3, GL_UNSIGNED_SHORT, 0);
//        glBindVertexArray(0);


//        glm::mat4 model;
//        model = glm::translate(model, glm::vec3(10.0f, 0.0f, 0.0f));
//        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
//        monModel.Draw(shader);

//        //glm::mat4 model2;

//        //model2 = glm::translate(model2, glm::vec3(-10.0, 0.0f, 0.0f));
//        //glUniformMatrix4fv(glGetUniformLocation(shader.Program, "model"), 1, GL_FALSE, glm::value_ptr(model2));
//        //monModel.Draw(shader);

//        glfwSwapBuffers(window);
//    }

//    glDeleteVertexArrays(1, &VAO);
//    glDeleteBuffers(1, &VBO);
//    glDeleteBuffers(1, &EBO);

//    glfwTerminate();
//    return 0;
//}


//#pragma endregion
