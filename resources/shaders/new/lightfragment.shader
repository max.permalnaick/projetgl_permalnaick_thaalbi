#version 330 core


in vec2 TexCoords;
in vec4 Vertexcolor;
out vec4 color;

uniform sampler2D modelTexture;

void main()
{
    color = texture(modelTexture, TexCoords);
}
