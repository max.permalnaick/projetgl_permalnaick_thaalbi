#version 330 core

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

out vec4 FragColor;

const int NB_LUMIERES=10;

uniform sampler2D modelTexture;
uniform vec3 viewPos;

struct Material
{
    vec3 ambient_;
    vec3 diffuse_;
    vec3 specular_;
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};
uniform Material material;

struct Light
{
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;
    bool enable;
};
uniform Light t_light[NB_LUMIERES];

//Pour gérer la lumiére sur l'objet
vec3 Radiation(Light _light);
vec3 Radiation2(Light _light);

void main()
{
    vec3 result;

    for(int i=0 ; i<NB_LUMIERES ; i++)
    {
	if(t_light[i].enable)
	{
	    result +=Radiation(t_light[i]);
	}
    }

    //vec4 texColor = texture(modelTexture, TexCoords) * vec4(result, 1.0f);
    vec4 FragColor_ = vec4(result, 1.0);
    if(FragColor_.a < 0.1)
	discard;
    FragColor = FragColor_;
}

vec3 Radiation(Light _light)
{
    // ambient
    vec3 ambient = _light.ambient * texture(material.diffuse, TexCoords).rgb;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(_light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = _light.diffuse * diff * texture(material.diffuse, TexCoords).rgb;

    // specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = _light.specular * spec * texture(material.specular, TexCoords).rgb;

    // spotlight (soft edges)
    float theta = dot(lightDir, normalize(-_light.direction));
    float epsilon = (_light.cutOff - _light.outerCutOff);
    float intensity = clamp((theta - _light.outerCutOff) / epsilon, 0.0, 1.0);
    diffuse  *= intensity;
    specular *= intensity;

    // attenuation
    float distance    = length(_light.position - FragPos);
    float attenuation = 1.0 / (_light.constant + _light.linear * distance + _light.quadratic * (distance * distance));
    ambient  *= attenuation;
    diffuse   *= attenuation;
    specular *= attenuation;

    vec3 result = ambient + diffuse + specular;

    return result;
}

vec3 Radiation2(Light _light)
{
    // ambient
    vec3 ambient = _light.ambient * material.ambient_;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(_light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = _light.diffuse * (diff * material.diffuse_);

    // specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = _light.specular * (spec * material.specular_);

    vec3 result = ambient + diffuse + specular;

    return result;
}
