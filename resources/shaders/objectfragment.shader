#version 330 core

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

out vec4 FragColor;

const int NB_LUMIERES=30;

uniform sampler2D modelTexture;
uniform vec3 viewPos;

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
};
uniform Material material;

struct Light {
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

//    vec3 color;
//    vec3 direction;
//    float cutOff;
//    float outerCutOff;
//    float constant;
//    float linear;
//    float quadratic;

    bool enable;

    vec3 direction;
    float cutOff;
    float outerCutOff;
    float constant;
    float linear;
    float quadratic;

};
uniform Light t_light[NB_LUMIERES];

//Pour gérer la lumiére sur l'objet
vec3 Radiation(Light _light);

void main()
{
    vec3 result;

    for(int i=0 ; i<NB_LUMIERES ; i++)
    {
	if(t_light[i].enable)
        {
            result +=Radiation(t_light[i]);
        }
    }

//    vec4 FragColor_ = texture(modelTexture, TexCoords) * vec4(result, 1.0f);
    vec4 FragColor_ =  vec4(result, 1.0f);
    if(FragColor_.a < 0.1)
        discard;
    FragColor = FragColor_;
}

//vec3 Radiation(Light _light)
//{
//    // ambient
//    vec3 ambient = _light.ambient * material.ambient;

//    // diffuse
//    vec3 norm = normalize(Normal);
//    vec3 lightDir = normalize(_light.position - FragPos);
//    float diff = max(dot(norm, lightDir), 0.0);
//    vec3 diffuse = _light.diffuse * (diff * material.diffuse);

//    // specular
//    vec3 viewDir = normalize(viewPos - FragPos);
//    vec3 reflectDir = reflect(-lightDir, norm);
//    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
//    vec3 specular = _light.specular * (spec * material.specular);

//    vec3 result = ambient + diffuse + specular;

//    return result;
//}

vec3 Radiation(Light _light)
{
    // ambient
    vec3 ambient = _light.ambient * texture(material.diffuse, TexCoords).rgb;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(_light.position - FragPos);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = _light.diffuse * diff * texture(material.diffuse, TexCoords).rgb;

    // specular
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = _light.specular * spec * texture(material.specular, TexCoords).rgb;

    vec3 result = ambient + diffuse + specular;

    return result;
}


//vec3 Radiation(Light light)
//{
//    // ambient
//    vec3 ambient = light.ambient * texture(material.diffuse, TexCoords).rgb;

//    // diffuse
//    vec3 norm = normalize(Normal);
//    vec3 lightDir = normalize(light.position - FragPos);
//    float diff = max(dot(norm, lightDir), 0.0);
//    vec3 diffuse = light.diffuse * diff * texture(material.diffuse, TexCoords).rgb;

//    // specular
//    vec3 viewDir = normalize(viewPos - FragPos);
//    vec3 reflectDir = reflect(-lightDir, norm);
//    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
//    vec3 specular = light.specular * spec * texture(material.specular, TexCoords).rgb;

//    // spotlight (soft edges)
//    float theta = dot(lightDir, normalize(-light.direction));
//    float epsilon = (light.cutOff - light.outerCutOff);
//    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
//    diffuse  *= intensity;
//    specular *= intensity;

//    // attenuation
//    float distance    = length(light.position - FragPos);
//    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
//    ambient  *= attenuation;
//    diffuse  *= attenuation;
//    specular *= attenuation;

//    vec3 result = ambient + diffuse + specular;

//    return result;
//}
