#version 330 core

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

out vec4 color;

const int NB_LUMIERES=10;

uniform sampler2D modelTexture;
uniform vec3 viewPos;

struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
};
uniform Material material;

struct Light {
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    bool enable;
};
uniform Light t_light[NB_LUMIERES];

//Pour gérer la lumiére sur l'objet
vec3 Radiation(vec3 _lightAmbient, vec3 _lightDiff, vec3 _lightSpec, vec3 _lightPos);

void main()
{
    vec3 result;

    for(int i=0 ; i<NB_LUMIERES ; i++)
    {
	if(t_light[i].enable)
	{
	    result +=Radiation(t_light[i].ambient,t_light[i].diffuse,t_light[i].specular,t_light[i].position);
	}
    }

    vec4 _color = vec4(result, 1.0f);
    if(_color.a < 0.1)
	discard;
    color = _color;
}

vec3 Radiation(vec3 _lightAmbient, vec3 _lightDiff, vec3 _lightSpec, vec3 _lightPos)
{
    // ambient
    vec3 ambient = _lightAmbient * material.ambient ;

    // diffuse
    vec3  norm = normalize(Normal);
    vec3  lightDir = normalize(_lightPos - FragPos);
    float diff = max (dot(norm, lightDir), 0.0);
    vec3  diffuse = _lightDiff * (diff * material.diffuse);

    // specular
    float specularStrength = 0.1;
    vec3  viewDir = normalize(viewPos - FragPos);
    vec3  reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3  specular = _lightSpec * (spec * material.specular);

    vec3 result = (ambient + diffuse +specular);

    return result;
}
