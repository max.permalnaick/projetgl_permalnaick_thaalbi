#version 330 core


in vec2 TexCoords;
out vec4 FragColor;

uniform sampler2D modelTexture;
uniform vec3 color;

void main()
{
    FragColor = texture(modelTexture, TexCoords)*vec4(color,1.);
}
