#version 330 core
layout (location = 0) in vec3 _position;
layout (location = 1) in vec3 _normal;
layout (location = 2) in vec2 _texCoords;

out vec2 TexCoords;
out vec3 Normal;
out vec3 FragPos;


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(_position, 1.0f);
    TexCoords = _texCoords;

    //projection du vertex dans le repere monde
    FragPos = vec3(model * vec4(_position, 1.0f));
    Normal = mat3( transpose( inverse(model) ) ) * _normal;
}

