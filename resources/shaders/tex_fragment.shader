

#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_normal1;

void main()
{    
    vec4 res = texture(texture_diffuse1, TexCoords);
//    res+=texture(texture_normal1, TexCoords);
    FragColor = res;
}


